package cn.itcast.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChineseServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * 结论： 约定很重要，网站一般采用UTF-8作为默认编码。如果不是特殊需求，不要变换成其他编码。
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*System.out.println("Before Set Request Encoding: " + request.getParameter("chinese"));
		request.setCharacterEncoding("UTF-8");
		System.out.println("After Set Request Encoding: " + request.getParameter("chinese"));

		response.setCharacterEncoding("UTF-8");*/
		response.getWriter().write(request.getParameter("chinese"));
	}
}
