<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="Acme.JPM.Encoders.GifEncoder" %>
<%@page import="java.awt.image.FilteredImageSource" %>
<%@page import="java.awt.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>r4r.co.in-ImageFilter</title>
</head>
<body>
<h1>ImageFilter Test page!</h1>
<form method="POST">
    <img src="java-logo1.gif" width="651"
         height="400" alt="Text Image"/><br/>
    <select name="applyingFilter">
        <option>ColorLess</option>
        <option>Black&White</option>
        <option>Red&Yellow</option>
        <option>DifferentColor</option>
    </select>
    <input type="submit" value="Applying Filter"/><br/>
    <a href="index.jsp">Return to Index.jsp page</a>
</form>
<%! java.awt.Frame frame = new java.awt.Frame();%>
<%
    // get requested image full path
    String imageName = request.getServletContext().getRealPath("java-logo1.gif");
    frame.addNotify();
    //create a dynamic image with specified image resource
    java.awt.Image image = java.awt.Toolkit.getDefaultToolkit().createImage(imageName);
    //Track the media object status
    java.awt.MediaTracker mt = new java.awt.MediaTracker(frame);
    mt.addImage(image, 0);
    // add image into list which track by MediaTracker
    mt.waitForAll(); //wait until all image not load into list
%>
<%
    // get drop-down field value from ImageFilter.jsp page
    String filterType = request.getParameter("applyingFilter");
    if (filterType != null) {
        if (filterType.equals("Colorless")) {
            /*create an ImageFilter which modifies
                the image pixels in according with RGB ColorModel. */
            java.awt.image.ImageFilter colorFilter =
                    new java.awt.image.RGBImageFilter() {
                        /*
                         *  More color combination for filter
                         *
                         * RGB --> YUV|  YUV --> RGB
                         * Y =  0.299*Red+0.587*Green+0.114*Blue | Red= Y+0.000*U+1.140*V
                         * U = -0.147*Red-0.289*Green+0.436*Blue | Green = Y-0.396*U-0.581*V
                         * V =  0.615*Red-0.515*Green-0.100*Blue | Blue  = Y+2.029*U+0.000*V
                         */
                        @Override
                        public int filterRGB(int x, int y, int rgb) {
                            // Get (Red Green Blue) color intensity value
                            int Red = (rgb & 0x00ff0000) >> 16;
                            int Green = (rgb & 0x0000ff00) >> 8;
                            int Blue = rgb & 0x000000ff;

                            int Luma = (int) (0.299 * Red + 0.587 * Green + 0.114 * Blue);
                            //Return image pixels value to ImageFilter
                            return ((0xff << 24) | (Luma << 16) | (Luma << 8) | Luma);
                        }
                    };
            /*Creates an offScreenImage from ImageProducer
                and applying ImageFilter on it.*/
            java.awt.Image offScreenImage =
                    frame.createImage(new FilteredImageSource(image.getSource(), colorFilter));
            ServletOutputStream sos = response.getOutputStream();
            // Encode offScreenImage into GIF format and send to client browser
            Acme.JPM.Encoders.GifEncoder encoder =
                    new GifEncoder(offScreenImage, sos);
            encoder.encode();
        } else if (filterType.equals("Black&White")) {
            java.awt.image.ImageFilter colorFilter =
                    new java.awt.image.RGBImageFilter() {

                        @Override
                        public int filterRGB(int x, int y, int rgb) {
                            int Red = (rgb & 0x00ff0000) >> 16;
                            int Green = (rgb & 0x0000ff00) >> 8;
                            int Blue = rgb & 0x000000ff;

                            int U = (int) (-0.147 * Red - 0.289 * Green + 0.436 * Blue);
                            return ((0xff << 24) | (U << 16) | (U << 8) | U);
                        }
                    };
            java.awt.Image offScreenImage =
                    frame.createImage(new FilteredImageSource(image.getSource(),
                            colorFilter));
            ServletOutputStream sos = response.getOutputStream();
            Acme.JPM.Encoders.GifEncoder encoder =
                    new GifEncoder(offScreenImage, sos);
            encoder.encode();
        } else if (filterType.equals("Red&Yellow")) {
            java.awt.image.ImageFilter colorFilter =
                    new java.awt.image.RGBImageFilter() {

                        @Override
                        public int filterRGB(int x, int y, int rgb) {

                            int Red = (rgb & 0x00ff0000) >> 16;
                            int Green = (rgb & 0x0000ff00) >> 8;
                            int Blue = rgb & 0x000000ff;

                            int Y = (int) (0.299 * Red + 0.587 * Green + 0.114 * Blue);
                            int U = (int) (-0.147 * Red - 0.289 * Green + 0.436 * Blue);
                            int V = (int) (0.615 * Red - 0.515 * Green - 0.100 * Blue);
                            return ((0xff << 24) | (Y << 16) | (U << 8) | V);

                        }
                    };
            java.awt.Image offScreenImage =
                    frame.createImage(new FilteredImageSource(image.getSource(),
                            colorFilter));
            ServletOutputStream sos = response.getOutputStream();
            Acme.JPM.Encoders.GifEncoder encoder =
                    new GifEncoder(offScreenImage, sos);
            encoder.encode();
        } else if (filterType.equals("DifferentColor")) {
            java.awt.image.ImageFilter colorFilter =
                    new java.awt.image.RGBImageFilter() {

                        @Override
                        public int filterRGB(int x, int y, int rgb) {
                            return ((rgb & 0xff00ff00)
                                    | ((rgb & 0xff0000) >> 16) | ((rgb & 0xff) << 16));
                        }
                    };
            java.awt.Image offScreenImage =
                    frame.createImage(new FilteredImageSource(image.getSource(),
                            colorFilter));
            ServletOutputStream sos = response.getOutputStream();
            Acme.JPM.Encoders.GifEncoder encoder =
                    new GifEncoder(offScreenImage, sos);
            encoder.encode();
        }
        frame.removeNotify(); // free resource
    }
%>
</body>
</html>
