<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>r4r.co.in-index</title>
</head>
<body>
<h1>Image Conversion Filter!</h1>
<form method="POST">
    <img src="java-logo1.gif" width="651" height="400" alt="java-logo1"/><br/>
    <select name="colorFilter">
        <option>Colorless</option>
        <option>Black&White</option>
        <option>DifferentColor</option>
    </select>
    <input type="submit" value="Applying Filter"/>
</form>
<br/>
<a href="ImageFilter.jsp">
    ImageFilter.jsp|Test purpose</a>
</body>
</html>
