<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>r4r.co.in-Encrypt</title>
</head>
<body>
<h1>Data encrypted and decrypted!</h1>
<!-- get attribute from request object -->
<p>Encrypted Data: <strong> ${encrypted}</strong></p>
<p>Decrypted Data: <strong> ${decrypted}</strong></p>
</body>
</html>
